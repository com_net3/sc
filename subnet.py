import ipaddress

# รับ IP address และ subnet mask จากผู้ใช้
ip_address = input("Enter IP address (e.g., 192.168.1.1): ")
subnet_mask = input("Enter Subnet Mask (e.g., 255.255.255.0 or /24): ")

try:
    # ใช้คลาส IPv4Network เพื่อคำนวณเครือข่าย
    network = ipaddress.IPv4Network(f"{ip_address}/{subnet_mask}", strict=False)

    # แสดงผลลัพธ์
    print(f"Network Address: {network.network_address}")
    print(f"Broadcast Address: {network.broadcast_address}")

except ValueError as e:
    print(f"Invalid input: {e}")
